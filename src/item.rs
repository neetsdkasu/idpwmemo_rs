extern crate java_data_io_rs;

use java_data_io_rs::{from_java_string, to_java_string, JavaDataInput, JavaDataOutput};
use std::convert::TryFrom;
use std::error;
use std::fmt;
use std::io;
use std::result;
use std::string;

const FORMAT_VERSION: i32 = 2;

pub(crate) type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    FormatError(String),
}

macro_rules! def_value_type {
    ($x:tt,) => (1);
    ($x:tt, $($y:tt,)*) => (1+def_value_type!($($y,)*));
    ($t:ty) => {
        impl TryFrom<$t> for ValueType {
            type Error = Error;
            fn try_from(v: $t) -> result::Result<Self, Self::Error> {
                VALUE_TYPES
                    .get(v as usize)
                    .copied()
                    .ok_or_else(|| Error::FormatError(format!("Invalid ValueType ({})", v)))
            }
        }
    };
    ($($t:ty;)*) => { $(def_value_type!($t);)* };
    ($($a:tt $b:tt $c:tt,)*) => {

        #[derive(Debug, Clone, Copy, PartialEq, Eq)]
        pub enum ValueType {
            $($a $b $c,)*
        }

        static VALUE_TYPES: [ValueType; def_value_type!($($a,)*)] = {
            use ValueType::*;
            let mut table = [
                $($a,)*
            ];
            $(table[$a as usize] = $a;)*
            table
        };

        def_value_type!(i8;i16;i32;i64;u8;u16;u32;u64;);
    };
}

def_value_type!(
    ServiceName = 0,
    ServiceUrl = 1,
    Id = 2,
    Password = 3,
    Email = 4,
    ReminderQuestion = 5,
    ReminderAnswer = 6,
    Description = 7,
);

impl fmt::Display for ValueType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use ValueType::*;
        match self {
            ServiceName => write!(f, "service name"),
            ServiceUrl => write!(f, "service url"),
            Id => write!(f, "id"),
            Password => write!(f, "password"),
            Email => write!(f, "email"),
            ReminderQuestion => write!(f, "reminder question"),
            ReminderAnswer => write!(f, "reminder answer"),
            Description => write!(f, "description"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Value {
    pub value_type: ValueType,
    pub value: String,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Service {
    pub time: i64,
    pub values: Vec<Value>,
    pub(crate) secrets: Vec<u8>,
}

#[derive(Debug)]
pub(crate) struct Memo {
    pub(crate) services: Vec<Service>,
}

impl Value {
    pub fn new(value_type: ValueType, value: &str) -> Self {
        Self {
            value_type,
            value: value.into(),
        }
    }

    pub(crate) fn save<W>(&self, dst: &mut JavaDataOutput<W>) -> Result<()>
    where
        W: io::Write,
    {
        dst.write_byte(self.value_type as i32)?;
        dst.write_utf(&to_java_string(&self.value))?;
        Ok(())
    }

    pub(crate) fn load<R>(src: &mut JavaDataInput<R>) -> Result<Value>
    where
        R: io::Read,
    {
        let t = src.read_byte()?;
        let s = src.read_utf()?;
        Ok(Value {
            value_type: ValueType::try_from(t)?,
            value: from_java_string(&s)?,
        })
    }

    pub fn is_empty(&self) -> bool {
        self.value.trim().is_empty()
    }
}

impl Service {
    pub fn new(name: &str) -> Self {
        let v = Value::new(ValueType::ServiceName, name);
        Self {
            time: 0,
            values: vec![v],
            secrets: Vec::new(),
        }
    }

    pub fn get_service_name(&self) -> &str {
        for v in self.values.iter() {
            if matches!(v.value_type, ValueType::ServiceName) {
                return &v.value;
            }
        }
        ""
    }

    pub(crate) fn save<W>(&self, dst: &mut JavaDataOutput<W>) -> Result<()>
    where
        W: io::Write,
    {
        dst.write_long(self.time)?;
        let values = Service::filter_values(&self.values);
        dst.write_int(values.len() as i32)?;
        for v in values {
            v.save(dst)?;
        }
        dst.write_int(self.secrets.len() as i32)?;
        dst.write(&self.secrets)?;
        Ok(())
    }

    pub(crate) fn load_v1<R>(src: &mut JavaDataInput<R>) -> Result<Service>
    where
        R: io::Read,
    {
        let vc = src.read_int()?;
        if vc < 0 {
            return Err(Error::FormatError(format!("invalid value count ({})", vc)));
        }
        let mut values: Vec<Value> = Vec::with_capacity(vc as usize);
        for _ in 0..vc {
            let v = Value::load(src)?;
            values.push(v);
        }
        let sc = src.read_int()?;
        if sc < 0 {
            return Err(Error::FormatError(format!("invalid secret size ({})", sc)));
        }
        let mut secrets: Vec<u8> = vec![0; sc as usize];
        src.read_fully(&mut secrets)?;
        Ok(Service {
            time: 0,
            values,
            secrets,
        })
    }

    pub(crate) fn load<R>(src: &mut JavaDataInput<R>) -> Result<Service>
    where
        R: io::Read,
    {
        let time = src.read_long()?;
        let vc = src.read_int()?;
        if vc < 0 {
            return Err(Error::FormatError(format!("invalid value count ({})", vc)));
        }
        let mut values: Vec<Value> = Vec::with_capacity(vc as usize);
        for _ in 0..vc {
            let v = Value::load(src)?;
            values.push(v);
        }
        let sc = src.read_int()?;
        if sc < 0 {
            return Err(Error::FormatError(format!("invalid secret size ({})", sc)));
        }
        let mut secrets: Vec<u8> = vec![0; sc as usize];
        src.read_fully(&mut secrets)?;
        Ok(Service {
            time,
            values,
            secrets,
        })
    }

    pub(crate) fn filter_values(values: &[Value]) -> Vec<&Value> {
        values.iter().filter(|v| !v.is_empty()).collect()
    }

    pub(crate) fn write_secrets<W>(dst: &mut JavaDataOutput<W>, values: &[&Value]) -> Result<()>
    where
        W: io::Write,
    {
        dst.write_int(values.len() as i32)?;
        for v in values {
            v.save(dst)?;
        }
        Ok(())
    }

    pub(crate) fn read_secrets<R>(src: &mut JavaDataInput<R>) -> Result<Vec<Value>>
    where
        R: io::Read,
    {
        let count = src.read_int()?;
        if count < 0 {
            return Err(Error::FormatError(format!(
                "invalid secret value count({})",
                count
            )));
        }
        let mut values: Vec<Value> = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let v = Value::load(src)?;
            values.push(v);
        }
        Ok(values)
    }
}

impl Memo {
    pub(crate) fn new(services: Vec<Service>) -> Self {
        Self { services }
    }

    pub(crate) fn save<W>(&self, dst: &mut JavaDataOutput<W>) -> Result<()>
    where
        W: io::Write,
    {
        dst.write_int(FORMAT_VERSION)?;
        let services: Vec<_> = self
            .services
            .iter()
            .filter(|s| !s.get_service_name().trim().is_empty())
            .collect();
        dst.write_int(services.len() as i32)?;
        for s in services {
            s.save(dst)?;
        }
        Ok(())
    }

    pub(crate) fn load<R>(src: &mut JavaDataInput<R>) -> Result<Memo>
    where
        R: io::Read,
    {
        let version = src.read_int()?;
        if !(0..=FORMAT_VERSION).contains(&version) {
            return Err(Error::FormatError(format!(
                "Unsupport Format Version ({})",
                version
            )));
        }
        let count = src.read_int()?;
        if count < 0 {
            return Err(Error::FormatError(format!(
                "invalid service count({})",
                count
            )));
        }
        let mut services: Vec<Service> = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let s = if version == 1 {
                Service::load_v1(src)?
            } else {
                Service::load(src)?
            };
            services.push(s);
        }
        Ok(Memo { services })
    }
}

impl From<java_data_io_rs::Error> for Error {
    fn from(err: java_data_io_rs::Error) -> Self {
        use java_data_io_rs::Error::IoError;
        match err {
            IoError(err) => Error::IoError(err),
            err => Error::FormatError(format!("{}", err)),
        }
    }
}

impl From<string::FromUtf16Error> for Error {
    fn from(err: string::FromUtf16Error) -> Self {
        Error::FormatError(format!("{}", err))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            IoError(ref e) => fmt::Display::fmt(e, f),
            FormatError(ref s) => write!(f, "FormatError({})", s),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use Error::*;
        match self {
            IoError(ref e) => Some(e),
            FormatError(_) => None,
        }
    }
}
