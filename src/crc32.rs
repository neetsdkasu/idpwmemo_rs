extern crate unkocrypto_rs;

use unkocrypto_rs::Checksum;

static CRC32_TABLE: [i64; 256] = {
    const fn f(c: i64) -> i64 {
        (c >> 1) ^ (0xedb88320 * (c & 1))
    }
    const fn calc(v: i64) -> i64 {
        f(f(f(f(f(f(f(f(v))))))))
    }
    let mut table = [0_i64; 256];
    macro_rules! m {
            ($e:expr) => {
                table[$e] = calc($e);
            };
            ($e:expr, $a:expr $(,$b:expr)*) => {
                m!(($e<<1) $(,$b)*);
                m!((($e<<1)|1) $(,$b)*);
            };
        }
    m!(0, 1, 2, 3, 4, 5, 6, 7, 8);
    table
};

pub(crate) struct Crc32 {
    value: i64,
}

impl Crc32 {
    pub(crate) fn new() -> Self {
        Self { value: 0xffffffff }
    }
}

impl Checksum for &mut Crc32 {
    fn reset(&mut self) {
        self.value = 0xffffffff;
    }

    fn update(&mut self, v: u8) {
        let b = CRC32_TABLE[(self.value & 0xff) as usize ^ v as usize];
        let c = self.value >> 8;
        self.value = b ^ c;
    }

    fn get_value(&self) -> i64 {
        self.value ^ 0xffffffff
    }
}
