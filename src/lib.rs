extern crate java_data_io_rs;

mod crc32;
mod cryptor;
mod item;

pub use crate::item::{Service, Value, ValueType};

use crate::cryptor::Cryptor;
use crate::item::Memo;
use java_data_io_rs::{JavaDataInput, JavaDataOutput};
use std::error;
use std::fmt;
use std::io;
use std::result;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    DataFormatError(String),
    CryptoError(String),
    RuntimeError(String),
}

#[derive(Default)]
pub struct IDPWMemo {
    cryptor: std::cell::RefCell<Cryptor>,
    memo: Option<Memo>,
    index: Option<usize>,
    secrets: Option<Vec<Value>>,
    password_v1: Option<Vec<u8>>,
    password_v2: Option<Vec<u8>>,
    version: i32,
}

impl IDPWMemo {
    pub fn set_password(&mut self, password: &[u8]) -> Result<()> {
        let mut cryptor = self.cryptor.borrow_mut();
        let enc_password = cryptor.encrypt_v1(&[], password)?;
        let key = cryptor.encrypt_v2(password, password)?;
        self.memo = None;
        self.index = None;
        self.secrets = None;
        self.password_v1 = Some(enc_password);
        self.password_v2 = Some(key);
        self.version = 0;
        Ok(())
    }

    fn get_password_v1(&self) -> Result<Vec<u8>> {
        let password = self
            .password_v1
            .as_ref()
            .ok_or_else(|| Error::RuntimeError("no password".into()))?;
        let password = self.cryptor.borrow_mut().decrypt_v1(&[], password)?;
        Ok(password)
    }

    fn get_password_v2(&self) -> Result<&Vec<u8>> {
        self.password_v2
            .as_ref()
            .ok_or_else(|| Error::RuntimeError("no password".into()))
    }

    pub fn new_memo(&mut self) -> Result<()> {
        if self.password_v1.is_none() {
            return Err(Error::RuntimeError("no password".into()));
        }
        let memo = Memo::new(Vec::new());
        self.memo = Some(memo);
        self.index = None;
        self.secrets = None;
        self.version = 2;
        Ok(())
    }

    pub fn load_memo(&mut self, src: &[u8]) -> Result<()> {
        let src_type = cryptor::check_src_type(src);
        if src_type == 0 {
            return Err(Error::RuntimeError("invalid source".into()));
        }
        let mut buf: Option<Vec<u8>> = None;
        if (src_type & 2) != 0 {
            let password = self.get_password_v2()?;
            match self.cryptor.borrow_mut().decrypt_v2(password, src) {
                Ok(res) => {
                    buf = Some(res);
                    self.version = 2;
                }
                Err(cryptor::Error::CryptoError(_)) if src_type != 2 => {}
                Err(error) => return Err(error.into()),
            }
        }
        if buf.is_none() && (src_type & 1) != 0 {
            let password = self.get_password_v1()?;
            buf = Some(
                self.cryptor
                    .borrow_mut()
                    .decrypt_repeat_v1(2, &password, src)?,
            );
            self.version = 1;
        }
        let mut jdi = buf.map(io::Cursor::new).map(JavaDataInput::new).unwrap();
        let memo = Memo::load(&mut jdi)?;
        self.memo = Some(memo);
        self.index = None;
        self.secrets = None;
        Ok(())
    }

    fn get_memo(&self) -> Result<&Memo> {
        self.memo
            .as_ref()
            .ok_or_else(|| Error::RuntimeError("no memo".into()))
    }

    fn get_memo_mut(&mut self) -> Result<&mut Memo> {
        self.memo
            .as_mut()
            .ok_or_else(|| Error::RuntimeError("no memo".into()))
    }

    pub fn services(&self) -> Result<&[Service]> {
        self.get_memo().map(|memo| memo.services.as_slice())
    }

    pub fn get_service_names(&self) -> Result<Vec<&str>> {
        self.get_memo().map(|memo| {
            memo.services
                .iter()
                .map(Service::get_service_name)
                .collect()
        })
    }

    pub fn select_service(&mut self, index: usize) -> Result<()> {
        self.save_secrets()?;
        self.get_memo().and_then(|memo| {
            if index < memo.services.len() {
                Ok(())
            } else {
                Err(Error::RuntimeError("wrong service index".into()))
            }
        })?;
        self.index = Some(index);
        self.secrets = None;
        Ok(())
    }

    pub fn add_new_service(&mut self, service_name: &str) -> Result<()> {
        self.save_secrets()?;
        let memo = self.get_memo_mut()?;
        let index = memo.services.len();
        let service = Service::new(service_name);
        memo.services.push(service);
        self.index = Some(index);
        self.secrets = None;
        Ok(())
    }

    pub fn get_index(&self) -> Result<usize> {
        self.index
            .ok_or_else(|| Error::RuntimeError("not select service".into()))
    }

    pub fn service(&self) -> Result<&Service> {
        let index = self.get_index()?;
        let memo = self.get_memo()?;
        Ok(&memo.services[index])
    }

    fn service_mut(&mut self) -> Result<&mut Service> {
        let index = self.get_index()?;
        let memo = self.get_memo_mut()?;
        Ok(&mut memo.services[index])
    }

    pub fn service_name(&self) -> Result<&str> {
        self.service().map(Service::get_service_name)
    }

    pub fn values(&self) -> Result<&[Value]> {
        let service = self.service()?;
        Ok(&service.values)
    }

    pub fn values_mut(&mut self) -> Result<&mut Vec<Value>> {
        let service = self.service_mut()?;
        Ok(&mut service.values)
    }

    pub fn secrets(&mut self) -> Result<&[Value]> {
        if let Some(ref secrets) = self.secrets {
            return Ok(secrets);
        }
        let index = self.get_index()?;
        let memo = self.get_memo()?;
        let sbuf = &memo.services[index].secrets;
        if sbuf.is_empty() {
            return Ok(&[]);
        }
        let buf: Vec<u8> = if self.version == 1 {
            let password = self.get_password_v1()?;
            self.cryptor
                .borrow_mut()
                .decrypt_repeat_v1(2, &password, sbuf)?
        } else {
            let password = self.get_password_v2()?;
            self.cryptor.borrow_mut().decrypt_v2(password, sbuf)?
        };
        let mut jdi = JavaDataInput::new(io::Cursor::new(buf));
        let secrets = Service::read_secrets(&mut jdi)?;
        self.secrets = Some(secrets);
        Ok(self.secrets.as_ref().unwrap())
    }

    pub fn secrets_mut(&mut self) -> Result<&mut Vec<Value>> {
        if self.secrets.is_some() {
            return Ok(self.secrets.as_mut().unwrap());
        }
        let index = self.get_index()?;
        let memo = self.get_memo()?;
        let sbuf = &memo.services[index].secrets;
        if sbuf.is_empty() {
            self.secrets = Some(Vec::new());
            return Ok(self.secrets.as_mut().unwrap());
        }
        let buf: Vec<u8> = if self.version == 1 {
            let password = self.get_password_v1()?;
            self.cryptor
                .borrow_mut()
                .decrypt_repeat_v1(2, &password, sbuf)?
        } else {
            let password = self.get_password_v2()?;
            self.cryptor.borrow_mut().decrypt_v2(password, sbuf)?
        };
        let mut jdi = JavaDataInput::new(io::Cursor::new(buf));
        let secrets = Service::read_secrets(&mut jdi)?;
        self.secrets = Some(secrets);
        Ok(self.secrets.as_mut().unwrap())
    }

    fn save_secrets(&mut self) -> Result<()> {
        let data = match self.secrets {
            None => return Ok(()),
            Some(ref vs) => {
                let vs: Vec<_> = Service::filter_values(vs);
                if vs.is_empty() {
                    Vec::with_capacity(0)
                } else {
                    let mut data: Vec<u8> = Vec::new();
                    let mut jdo = JavaDataOutput::new(&mut data);
                    Service::write_secrets(&mut jdo, &vs)?;
                    data
                }
            }
        };
        if data.is_empty() {
            let index = self.index.unwrap();
            let memo = self.memo.as_mut().unwrap();
            memo.services[index].secrets = data;
            return Ok(());
        }
        let sbuf = if self.version == 1 {
            let password = self.get_password_v1()?;
            self.cryptor
                .borrow_mut()
                .encrypt_repeat_v1(2, &password, &data)?
        } else {
            let password = self.get_password_v2()?;
            self.cryptor.borrow_mut().encrypt_v2(password, &data)?
        };
        let index = self.index.unwrap();
        let memo = self.memo.as_mut().unwrap();
        memo.services[index].secrets = sbuf;
        Ok(())
    }

    pub fn save(&mut self) -> Result<Vec<u8>> {
        self.save_secrets()?;
        let memo = self.get_memo()?;
        let mut data: Vec<u8> = Vec::new();
        let mut jdo = JavaDataOutput::new(&mut data);
        memo.save(&mut jdo)?;
        let res = if self.version == 1 {
            let password = self.get_password_v1()?;
            self.cryptor
                .borrow_mut()
                .encrypt_repeat_v1(2, &password, &data)?
        } else {
            let password = self.get_password_v2()?;
            self.cryptor.borrow_mut().encrypt_v2(password, &data)?
        };
        Ok(res)
    }

    pub fn change_password(&mut self, new_password: &[u8]) -> Result<()> {
        self.save_secrets()?;
        let password = if self.version == 1 {
            self.get_password_v1()?
        } else {
            Vec::with_capacity(0)
        };
        let password = if self.version == 1 {
            &password
        } else {
            self.get_password_v2()?
        };
        let mut cryptor = self.cryptor.borrow_mut();
        let new_key = cryptor.encrypt_v2(new_password, new_password)?;
        let memo = self.get_memo()?;
        let len = memo.services.len();
        let mut tmp: Vec<Vec<u8>> = Vec::with_capacity(len);
        for i in 0..len {
            let tbuf = &memo.services[i].secrets;
            if tbuf.is_empty() {
                tmp.push(Vec::with_capacity(0));
            } else if self.version == 1 {
                let buf2 = cryptor.decrypt_repeat_v1(2, password, tbuf)?;
                let sbuf = cryptor.encrypt_repeat_v1(2, new_password, &buf2)?;
                tmp.push(sbuf);
            } else {
                let buf2 = cryptor.decrypt_v2(password, tbuf)?;
                let sbuf = cryptor.encrypt_v2(&new_key, &buf2)?;
                tmp.push(sbuf);
            }
        }
        let new_enc_password = cryptor.encrypt_v1(&[], new_password)?;
        std::mem::drop(cryptor);
        let memo = self.get_memo_mut()?;
        for (i, b) in tmp.into_iter().enumerate() {
            memo.services[i].secrets = b;
        }
        self.password_v1 = Some(new_enc_password);
        self.password_v2 = Some(new_key);
        Ok(())
    }
}

impl From<cryptor::Error> for Error {
    fn from(error: cryptor::Error) -> Self {
        use cryptor::Error::*;
        match error {
            IoError(error) => Error::IoError(error),
            CryptoError(s) => Error::CryptoError(s),
        }
    }
}

impl From<item::Error> for Error {
    fn from(error: item::Error) -> Self {
        use item::Error::*;
        match error {
            IoError(error) => Error::IoError(error),
            FormatError(s) => Error::DataFormatError(s),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            IoError(ref e) => fmt::Display::fmt(e, f),
            DataFormatError(ref s) => write!(f, "DataFormatError({})", s),
            CryptoError(ref s) => write!(f, "CryptoError({})", s),
            RuntimeError(ref s) => write!(f, "RuntimeError({})", s),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use Error::*;
        match self {
            IoError(ref e) => Some(e),
            DataFormatError(_) => None,
            CryptoError(_) => None,
            RuntimeError(_) => None,
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {
        // item::FORMAT_VERSION = 1, cryptor::VERSION = 1
        let sample1_memo: [u8; 338] = [
            136, 173, 116, 165, 68, 204, 83, 146, 46, 246, 99, 149, 105, 124, 25, 219, 104, 214,
            14, 224, 183, 218, 98, 40, 198, 103, 164, 24, 246, 151, 255, 121, 163, 66, 76, 237, 94,
            65, 87, 253, 136, 252, 165, 119, 235, 114, 9, 27, 58, 231, 52, 69, 136, 183, 114, 221,
            172, 142, 89, 24, 251, 167, 245, 154, 211, 243, 20, 169, 245, 236, 235, 190, 70, 97,
            12, 151, 2, 193, 191, 88, 75, 206, 53, 71, 102, 140, 61, 187, 190, 146, 218, 11, 28,
            225, 137, 180, 32, 37, 66, 149, 52, 248, 112, 204, 247, 26, 2, 203, 150, 79, 78, 32,
            83, 131, 147, 204, 85, 105, 165, 160, 137, 151, 62, 56, 125, 173, 251, 182, 224, 171,
            58, 86, 77, 135, 62, 197, 222, 244, 149, 41, 175, 91, 96, 35, 2, 143, 215, 190, 40,
            128, 83, 238, 171, 146, 181, 150, 189, 52, 95, 82, 61, 63, 42, 250, 60, 168, 210, 90,
            115, 211, 244, 226, 25, 66, 228, 104, 159, 9, 125, 240, 212, 239, 146, 31, 56, 172,
            213, 65, 218, 178, 205, 207, 163, 230, 142, 175, 102, 100, 197, 203, 106, 183, 224, 92,
            168, 153, 81, 28, 112, 16, 130, 42, 70, 74, 220, 232, 12, 120, 184, 3, 125, 71, 186,
            89, 141, 88, 191, 211, 63, 52, 145, 92, 115, 62, 108, 148, 127, 102, 199, 195, 169, 24,
            9, 55, 48, 73, 105, 95, 195, 9, 206, 150, 137, 32, 217, 192, 106, 152, 96, 37, 128,
            161, 70, 227, 64, 32, 57, 87, 152, 155, 21, 96, 122, 252, 227, 122, 37, 31, 37, 143,
            53, 134, 94, 70, 146, 142, 156, 200, 139, 143, 227, 41, 235, 179, 159, 181, 190, 150,
            86, 160, 86, 51, 73, 202, 56, 58, 25, 113, 13, 142, 4, 83, 238, 181, 186, 16, 237, 253,
            139, 225, 63, 136, 242, 87, 3, 202, 242, 122, 217, 20, 8, 69, 226, 14, 240, 246, 179,
            71,
        ];

        // item::FORMAT_VERSION = 2, cryptor::VERSION = 2
        let sample2_memo: [u8; 365] = [
            45, 220, 210, 211, 50, 250, 55, 13, 22, 9, 156, 165, 192, 59, 63, 40, 80, 220, 133, 44,
            62, 186, 253, 136, 76, 161, 28, 117, 80, 90, 144, 254, 234, 189, 156, 216, 229, 133,
            53, 251, 174, 175, 193, 222, 42, 180, 75, 181, 85, 93, 120, 172, 209, 139, 40, 37, 31,
            169, 62, 169, 216, 98, 128, 54, 37, 147, 216, 220, 124, 75, 199, 254, 145, 88, 191,
            217, 34, 135, 63, 231, 201, 253, 243, 89, 185, 141, 35, 13, 58, 213, 68, 178, 225, 240,
            21, 131, 85, 171, 245, 6, 171, 82, 21, 183, 191, 207, 108, 247, 140, 125, 65, 217, 244,
            233, 147, 182, 65, 150, 63, 179, 71, 8, 126, 177, 177, 178, 205, 60, 63, 23, 232, 143,
            192, 115, 207, 172, 81, 90, 195, 141, 186, 201, 36, 63, 253, 155, 36, 225, 246, 224,
            180, 74, 120, 130, 79, 161, 40, 187, 199, 172, 218, 177, 112, 47, 178, 138, 135, 6,
            115, 35, 40, 222, 86, 148, 58, 35, 24, 195, 90, 245, 116, 74, 71, 214, 205, 244, 129,
            236, 84, 180, 54, 116, 230, 253, 138, 137, 8, 227, 138, 108, 178, 185, 56, 252, 20,
            164, 165, 173, 79, 44, 128, 180, 136, 2, 20, 130, 154, 232, 59, 250, 161, 202, 11, 224,
            191, 83, 136, 8, 198, 103, 52, 208, 169, 13, 130, 85, 177, 40, 171, 30, 118, 206, 68,
            168, 65, 209, 18, 40, 183, 96, 172, 199, 246, 35, 144, 178, 139, 189, 179, 63, 78, 173,
            63, 81, 34, 66, 253, 129, 143, 40, 195, 18, 215, 110, 47, 165, 236, 146, 198, 22, 100,
            14, 183, 253, 220, 77, 111, 167, 46, 215, 148, 163, 236, 179, 55, 67, 159, 189, 109,
            65, 24, 1, 12, 61, 84, 97, 117, 34, 32, 239, 108, 227, 174, 227, 83, 28, 247, 15, 0,
            11, 205, 76, 247, 60, 6, 117, 23, 159, 16, 129, 151, 90, 37, 207, 99, 64, 58, 3, 167,
            28, 105, 11, 34, 202, 123, 152, 235, 1, 95, 17, 66, 107, 118, 53, 227, 86, 72, 2, 206,
            36, 154, 87, 210, 167, 110,
        ];

        let password = "さんぷるdayo";

        let mut idpw_memo: crate::IDPWMemo = Default::default();
        idpw_memo.set_password(password.as_bytes()).unwrap();
        idpw_memo.load_memo(&sample2_memo).unwrap();

        {
            let service_names = idpw_memo.get_service_names().unwrap();
            let mut iter = service_names.iter();
            assert_eq!(iter.next(), Some(&"SAMPLE Service"));
            assert_eq!(iter.next(), Some(&"サンプル！"));
            assert_eq!(iter.next(), Some(&"simple-sample"));
            assert_eq!(iter.next(), None);
        }

        idpw_memo.select_service(0).unwrap();
        assert_eq!(idpw_memo.service_name().unwrap(), "SAMPLE Service");
        {
            use crate::ValueType::*;
            let val = crate::Value::new;
            let values = idpw_memo.values().unwrap();
            let mut iter = values.iter();
            assert_eq!(iter.next(), Some(&val(ServiceName, "SAMPLE Service")));
            assert_eq!(iter.next(), Some(&val(Id, "user1")));
            assert_eq!(iter.next(), Some(&val(ServiceUrl, "http://example.com")));
            assert_eq!(iter.next(), Some(&val(Email, "user1@example.com")));
            assert_eq!(iter.next(), None);

            let secrets = idpw_memo.secrets().unwrap();
            let mut iter = secrets.iter();
            assert_eq!(iter.next(), Some(&val(Password, "Password1")));
            assert_eq!(iter.next(), None);
        }

        idpw_memo.select_service(1).unwrap();
        assert_eq!(idpw_memo.service_name().unwrap(), "サンプル！");
        {
            use crate::ValueType::*;
            let val = crate::Value::new;
            let values = idpw_memo.values().unwrap();
            let mut iter = values.iter();
            assert_eq!(iter.next(), Some(&val(ServiceName, "サンプル！")));
            assert_eq!(iter.next(), Some(&val(Id, "ゆーざ１")));
            assert_eq!(iter.next(), Some(&val(Description, "さんぷるなんだよ")));
            assert_eq!(iter.next(), None);

            let secrets = idpw_memo.secrets().unwrap();
            let mut iter = secrets.iter();
            assert_eq!(iter.next(), Some(&val(Password, "ぱすわーど１")));
            assert_eq!(
                iter.next(),
                Some(&val(ReminderQuestion, "あなたの国籍は？"))
            );
            assert_eq!(iter.next(), Some(&val(ReminderAnswer, "日本！")));
            assert_eq!(iter.next(), None);
        }

        idpw_memo.select_service(2).unwrap();
        assert_eq!(idpw_memo.service_name().unwrap(), "simple-sample");
        {
            use crate::ValueType::*;
            let val = crate::Value::new;
            let values = idpw_memo.values().unwrap();
            let mut iter = values.iter();
            assert_eq!(iter.next(), Some(&val(ServiceName, "simple-sample")));
            assert_eq!(iter.next(), Some(&val(Id, "UserX")));
            assert_eq!(iter.next(), None);

            let secrets = idpw_memo.secrets().unwrap();
            let mut iter = secrets.iter();
            assert_eq!(iter.next(), None);
        }

        idpw_memo.select_service(1).unwrap();
        assert_eq!(idpw_memo.secrets().unwrap().len(), 3);

        let buf = idpw_memo.save().unwrap();
        assert_eq!(&buf, &sample2_memo);

        // make new

        let mut idpw_memo: crate::IDPWMemo = Default::default();
        idpw_memo.set_password(password.as_bytes()).unwrap();
        idpw_memo.new_memo().unwrap();

        idpw_memo.add_new_service("SAMPLE Service").unwrap();
        {
            use crate::ValueType::*;
            let val = crate::Value::new;

            let values = idpw_memo.values_mut().unwrap();
            values.push(val(Id, "user1"));
            values.push(val(ServiceUrl, "http://example.com"));
            values.push(val(Email, "user1@example.com"));

            let secrets = idpw_memo.secrets_mut().unwrap();
            secrets.push(val(Password, "Password1"));
        }

        idpw_memo.add_new_service("サンプル！").unwrap();
        {
            use crate::ValueType::*;
            let val = crate::Value::new;

            let values = idpw_memo.values_mut().unwrap();
            values.push(val(Id, "ゆーざ１"));
            values.push(val(Description, "さんぷるなんだよ"));

            let secrets = idpw_memo.secrets_mut().unwrap();
            secrets.push(val(Password, "ぱすわーど１"));
            secrets.push(val(ReminderQuestion, "あなたの国籍は？"));
            secrets.push(val(ReminderAnswer, "日本！"));
        }

        idpw_memo.add_new_service("simple-sample").unwrap();
        {
            use crate::ValueType::*;
            let val = crate::Value::new;

            let values = idpw_memo.values_mut().unwrap();
            values.push(val(Id, "UserX"));

            let secrets = idpw_memo.secrets_mut().unwrap();
            secrets.clear();
        }

        let buf = idpw_memo.save().unwrap();
        assert_eq!(&buf, &sample2_memo);

        // change password

        let old_password = password;
        let new_password = "ふーばーばず！ foo bar baz!";

        idpw_memo.change_password(new_password.as_bytes()).unwrap();

        let x_buf = idpw_memo.save().unwrap();
        assert_ne!(&x_buf, &sample2_memo);

        let mut idpw_memo: crate::IDPWMemo = Default::default();

        idpw_memo.set_password(new_password.as_bytes()).unwrap();

        idpw_memo.load_memo(&x_buf).unwrap();

        idpw_memo.select_service(0).unwrap();
        assert_eq!(idpw_memo.service_name().unwrap(), "SAMPLE Service");
        {
            use crate::ValueType::*;
            let val = crate::Value::new;
            let values = idpw_memo.values().unwrap();
            let mut iter = values.iter();
            assert_eq!(iter.next(), Some(&val(ServiceName, "SAMPLE Service")));
            assert_eq!(iter.next(), Some(&val(Id, "user1")));
            assert_eq!(iter.next(), Some(&val(ServiceUrl, "http://example.com")));
            assert_eq!(iter.next(), Some(&val(Email, "user1@example.com")));
            assert_eq!(iter.next(), None);

            let secrets = idpw_memo.secrets().unwrap();
            let mut iter = secrets.iter();
            assert_eq!(iter.next(), Some(&val(Password, "Password1")));
            assert_eq!(iter.next(), None);
        }

        idpw_memo.change_password(old_password.as_bytes()).unwrap();

        let buf = idpw_memo.save().unwrap();
        assert_eq!(&buf, &sample2_memo);

        // load v1 memo data

        let mut idpw_memo_v1: crate::IDPWMemo = Default::default();
        idpw_memo_v1.set_password(password.as_bytes()).unwrap();
        idpw_memo_v1.load_memo(&sample1_memo).unwrap();

        let mut idpw_memo_v2: crate::IDPWMemo = Default::default();
        idpw_memo_v2.set_password(password.as_bytes()).unwrap();
        idpw_memo_v2.load_memo(&sample2_memo).unwrap();

        let services1 = idpw_memo_v1.services().unwrap();
        let services2 = idpw_memo_v2.services().unwrap();

        assert_eq!(services1.len(), services2.len());

        let len = services1.len();

        for i in 0..len {
            idpw_memo_v1.select_service(i).unwrap();
            idpw_memo_v2.select_service(i).unwrap();

            let service1 = idpw_memo_v1.service().unwrap();
            let service2 = idpw_memo_v2.service().unwrap();
            assert_eq!(service1.time, service2.time);

            let values1 = idpw_memo_v1.values().unwrap();
            let values2 = idpw_memo_v2.values().unwrap();
            assert_eq!(values1, values2);

            let secrets1 = idpw_memo_v1.secrets().unwrap();
            let secrets2 = idpw_memo_v2.secrets().unwrap();
            assert_eq!(secrets1, secrets2);
        }
    }
}
