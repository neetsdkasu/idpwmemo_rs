extern crate mersenne_twister_rs;
extern crate unkocrypto_rs;

use crate::crc32::Crc32;
use std::error;
use std::fmt;
use std::io;
use std::result;
use unkocrypto_rs::{IntRng, MIN_BLOCK_SIZE};

const MAX_BLOCK_SIZE: usize = 1024;
const VERSION: u8 = 2;

pub(crate) type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub(crate) enum Error {
    IoError(io::Error),
    CryptoError(String),
}

#[derive(Default)]
pub(crate) struct Cryptor {
    rand: MtRandom,
}

#[derive(Default)]
struct MtRandom {
    mt: Box<mersenne_twister_rs::MersenneTwister>,
}

impl MtRandom {
    fn set_seed(&mut self, seed: &[u32]) {
        self.mt.init_by_array(seed);
    }
}

impl IntRng for MtRandom {
    fn next_int(&mut self) -> i32 {
        self.mt.genrand_u32() as i32
    }
}

fn gen_initial_seed(size: usize) -> Vec<u32> {
    let mut seed: Vec<u32> = vec![0; size];
    seed[0] = 0x98765432;
    seed[1] = 0xF1E2D3C4;
    for i in 2..seed.len() {
        seed[i] =
            seed[i - 2] ^ (seed[i - 1] >> ((i - 1) & 0xF)) ^ (seed[i - 1] << ((i + i + 1) & 0xF));
    }
    seed
}

fn gen_seed_v1(password: &[u8]) -> Vec<u32> {
    if password.is_empty() {
        return gen_initial_seed(23);
    }
    let mut seed = gen_initial_seed(password.len() + 13);
    let mut p: usize = 0;
    for (i, s) in seed.iter_mut().enumerate() {
        for j in 0..4 {
            *s |= (((password[p] as i8) as i32) << (((j + i * i) & 3) << 3)) as u32;
            p += 1;
            if p >= password.len() {
                p = 0;
            }
        }
    }
    seed
}

fn encrypt_block_size(src_len: usize) -> usize {
    use unkocrypto_rs::META_SIZE;
    let size = MIN_BLOCK_SIZE.max(src_len + META_SIZE);
    if size <= MAX_BLOCK_SIZE {
        return size;
    }
    let mut size = MIN_BLOCK_SIZE;
    let mut block_count = (src_len + (size - META_SIZE) - 1) / (size - META_SIZE);
    let mut total_size = size * block_count;
    for sz in MIN_BLOCK_SIZE + 1..=MAX_BLOCK_SIZE {
        block_count = (src_len + (sz - META_SIZE) - 1) / (sz - META_SIZE);
        if sz * block_count < total_size {
            size = sz;
            total_size = sz * block_count;
        }
    }
    size
}

impl Cryptor {
    pub(crate) fn decrypt_v1(&mut self, password: &[u8], src: &[u8]) -> Result<Vec<u8>> {
        use unkocrypto_rs::Error::UnkocryptoError;
        let rand = &mut self.rand;
        let seed = gen_seed_v1(password);
        let mut size = MAX_BLOCK_SIZE.min(src.len());
        let mut cs = Crc32::new();
        let mut dst: Vec<u8> = Vec::new();
        while size >= MIN_BLOCK_SIZE {
            if src.len() % size != 0 {
                size -= 1;
                continue;
            }
            let mut cur = io::Cursor::new(src);
            rand.set_seed(&seed);
            dst.clear();
            match unkocrypto_rs::decrypt(size, &mut cs, rand, &mut cur, &mut dst) {
                Ok(_) => return Ok(dst),
                Err(UnkocryptoError(_)) => {}
                Err(error) => return Err(error.into()),
            }
            size -= 1;
        }
        Err(Error::CryptoError("wrong password or source".into()))
    }

    pub(crate) fn encrypt_v1(&mut self, password: &[u8], src: &[u8]) -> Result<Vec<u8>> {
        let rand = &mut self.rand;
        let size = encrypt_block_size(src.len());
        let seed = gen_seed_v1(password);
        let mut cs = Crc32::new();
        let mut cur = io::Cursor::new(src);
        let mut dst: Vec<u8> = Vec::new();
        rand.set_seed(&seed);
        unkocrypto_rs::encrypt(size, &mut cs, rand, &mut cur, &mut dst)?;
        Ok(dst)
    }

    pub(crate) fn decrypt_repeat_v1(
        &mut self,
        n: i32,
        password: &[u8],
        src: &[u8],
    ) -> Result<Vec<u8>> {
        let mut buf: Vec<u8> = Vec::with_capacity(0);
        let mut src = src;
        for _ in 0..n {
            buf = self.decrypt_v1(password, src)?;
            src = &buf;
        }
        Ok(buf)
    }

    pub(crate) fn encrypt_repeat_v1(
        &mut self,
        n: i32,
        password: &[u8],
        src: &[u8],
    ) -> Result<Vec<u8>> {
        let mut buf: Vec<u8> = Vec::with_capacity(0);
        let mut src = src;
        for _ in 0..n {
            buf = self.encrypt_v1(password, src)?;
            src = &buf;
        }
        Ok(buf)
    }
}

pub(crate) fn check_src_type(src: &[u8]) -> i32 {
    let mut src_type = 0;
    let version: u8 = src.iter().take(8).fold(0, |a, x| a ^ *x);
    if version == VERSION {
        let data_len = src.len() - 1;
        let mut size = MAX_BLOCK_SIZE.min(data_len);
        while size >= MIN_BLOCK_SIZE {
            if data_len % size == 0 {
                src_type = 2;
                break;
            }
            size -= 1;
        }
    }
    let mut size = MAX_BLOCK_SIZE.min(src.len());
    while size >= MIN_BLOCK_SIZE {
        if src.len() % size == 0 {
            src_type |= 1;
            break;
        }
        size -= 1;
    }
    src_type
}

fn gen_seed_v2(password: &[u8]) -> Vec<u32> {
    if password.is_empty() {
        return gen_initial_seed(37);
    }
    let mut seed = gen_initial_seed(password.len() + 29);
    let mut p: usize = 0;
    for (i, s) in seed.iter_mut().enumerate() {
        for j in 0..4 {
            *s ^= (password[p] as u32) << (((j + i * i) & 3) << 3);
            p += 1;
            if p >= password.len() {
                p = 0;
            }
        }
    }
    seed
}

impl Cryptor {
    pub(crate) fn decrypt_v2(&mut self, password: &[u8], src: &[u8]) -> Result<Vec<u8>> {
        use unkocrypto_rs::Error::UnkocryptoError;
        let version: u8 = src.iter().take(8).fold(0, |a, x| a ^ *x);
        if version != VERSION {
            return Err(Error::CryptoError("wrong source".into()));
        }
        let rand = &mut self.rand;
        let seed = gen_seed_v2(password);
        let src = &src[1..];
        let mut size1 = MAX_BLOCK_SIZE.min(src.len());
        let mut cs = Crc32::new();
        let mut dst1: Vec<u8> = Vec::new();
        let mut dst2: Vec<u8> = Vec::new();
        while size1 >= MIN_BLOCK_SIZE {
            if src.len() % size1 != 0 {
                size1 -= 1;
                continue;
            }
            let mut cur = io::Cursor::new(src);
            rand.set_seed(&seed);
            dst1.clear();
            match unkocrypto_rs::decrypt(size1, &mut cs, rand, &mut cur, &mut dst1) {
                Ok(_) => {}
                Err(UnkocryptoError(_)) => {
                    size1 -= 1;
                    continue;
                }
                Err(error) => return Err(error.into()),
            }
            let mut size2 = MAX_BLOCK_SIZE.min(dst1.len());
            while size2 >= MIN_BLOCK_SIZE {
                if dst1.len() % size2 != 0 {
                    size2 -= 1;
                    continue;
                }
                let mut cur2 = io::Cursor::new(&mut dst1);
                rand.set_seed(&seed);
                dst2.clear();
                match unkocrypto_rs::decrypt(size2, &mut cs, rand, &mut cur2, &mut dst2) {
                    Ok(_) => return Ok(dst2),
                    Err(UnkocryptoError(_)) => {}
                    Err(error) => return Err(error.into()),
                }
                size2 -= 1;
            }
            size1 -= 1;
        }
        Err(Error::CryptoError("wrong password or source".into()))
    }

    pub(crate) fn encrypt_v2(&mut self, password: &[u8], src: &[u8]) -> Result<Vec<u8>> {
        let rand = &mut self.rand;
        let size = encrypt_block_size(src.len());
        let seed = gen_seed_v2(password);
        let mut cs = Crc32::new();
        let mut cur = io::Cursor::new(src);
        let mut dst1: Vec<u8> = Vec::new();
        rand.set_seed(&seed);
        unkocrypto_rs::encrypt(size, &mut cs, rand, &mut cur, &mut dst1)?;
        let size = encrypt_block_size(dst1.len());
        let mut cur = io::Cursor::new(dst1);
        let mut dst2: Vec<u8> = vec![VERSION];
        rand.set_seed(&seed);
        unkocrypto_rs::encrypt(size, &mut cs, rand, &mut cur, &mut dst2)?;
        dst2[0] = dst2.iter().take(8).fold(0, |a, x| a ^ *x);
        Ok(dst2)
    }
}

impl From<unkocrypto_rs::Error> for Error {
    fn from(error: unkocrypto_rs::Error) -> Self {
        use unkocrypto_rs::Error::*;
        match error {
            IoError(error) => Error::IoError(error),
            UnkocryptoError(c) => Error::CryptoError(format!("{}", c)),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            IoError(ref e) => fmt::Display::fmt(e, f),
            CryptoError(ref s) => write!(f, "CryptoError({})", s),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use Error::*;
        match self {
            IoError(ref e) => Some(e),
            CryptoError(_) => None,
        }
    }
}
